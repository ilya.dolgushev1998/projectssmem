package com.morozov.feature_authorization_impl.ui.fragments.phone

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.NavOptions
import androidx.navigation.Navigation
import com.morozov.core_backend_api.authorization.AuthBodyCreator
import com.morozov.feature_authorization_impl.R
import com.morozov.feature_authorization_impl.ui.MainObject
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_phone_number.*
import okhttp3.FormBody

class PhoneNumberFragment: Fragment() {
    companion object{
        const val TAG = "PhoneNumberFragment_TAG"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_phone_number, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        editTextNumber.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                isNumberReady()
            }
        })

        buttonSendCode.setOnClickListener {
            val backendApi = MainObject.mBackendApi ?: return@setOnClickListener

            val body = AuthBodyCreator.sendPhoneBody("7${editTextNumber.rawText}")
            backendApi.authApi().sendPhone(body)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ },{
                    it.printStackTrace()
                })
            val bundle = Bundle()
            bundle.putString("phoneNumber", "7${editTextNumber.rawText}")
            Navigation.findNavController(view).navigate(R.id.action_phoneNumberFragment_to_smsCodeFragment, bundle)
        }
    }

    private fun isNumberReady() {
        if (editTextNumber.text?.length == "(###)###-##-##".length) {
            buttonSendCode.isEnabled = true
            buttonSendCode.backgroundTintList = context?.resources?.getColorStateList(R.color.colorPrimary)
        } else {
            buttonSendCode.isEnabled = false
            buttonSendCode.backgroundTintList = context?.resources?.getColorStateList(R.color.auth_light_grey)
        }
    }
}