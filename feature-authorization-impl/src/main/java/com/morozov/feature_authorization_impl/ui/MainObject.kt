package com.morozov.feature_authorization_impl.ui

import com.morozov.core_backend_api.FeatureBackendApi
import com.morozov.feature_authorization_api.FeatureAuthorizationCallback

object MainObject {
    
    var mCallback: FeatureAuthorizationCallback? = null
    var mBackendApi: FeatureBackendApi? = null
}