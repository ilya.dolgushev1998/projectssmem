package com.morozov.feature_authorization_impl.ui.fragments.sms

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context.INPUT_METHOD_SERVICE
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.morozov.core_backend_api.authorization.AuthBodyCreator
import com.morozov.core_backend_api.token.TokenPreferences
import com.morozov.feature_authorization_impl.R
import com.morozov.feature_authorization_impl.ui.MainObject
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_sms_code.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import okhttp3.FormBody

class SmsCodeFragment: Fragment() {
    companion object{
        const val TAG = "SmsCodeFragment_TAG"
        private const val trueCode = "1234"
    }

    private var userCode = ""
    private var currentNumber = 0
    private var beforeText = ""
    private var afterText = ""

    private val timerSeconds = MutableLiveData<Int>()

    private lateinit var mPhoneNumber: String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_sms_code, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mPhoneNumber = arguments?.getString("phoneNumber") ?: "79531405321"

        arrowBack.setOnClickListener {
            Navigation.findNavController(view).popBackStack()
        }

        editNumbers.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                s ?: return
                beforeText = afterText
                afterText = s.toString()
                if (beforeText.length < afterText.length && currentNumber < 4) {
                    showEditNumber(currentNumber, afterText[afterText.length - 1].toString())
                    currentNumber++
                }
                if (beforeText.length > afterText.length && currentNumber > 0) {
                    currentNumber--
                    hideEditNumber(currentNumber)
                }
            }
        })

        timerSeconds.observe(this, Observer<Int> {
            if (it == null) {
                linearNewCode.visibility = View.VISIBLE
                linearWait.visibility = View.INVISIBLE
            } else {
                textSeconds.text = it.toString()
            }
        })

        linearNewCode.setOnClickListener {
            getNewCode()
        }
    }

    // Helper funcs
    private fun getNewCode() {
        linearNewCode.visibility = View.INVISIBLE
        linearWait.visibility = View.VISIBLE
        startWaitTimer()
        getNewSmsCode()
    }

    private fun startWaitTimer() {
        GlobalScope.launch(Dispatchers.IO) {
            for (i in 0..30) {
                timerSeconds.postValue(i+1)
                delay(1000)
            }
            timerSeconds.postValue(null)
        }
    }

    // Number functions
    private fun showEditNumber(edit: Int, number: String) {
        when (edit) {
            0 -> {
                showNumber(ellipse1, number1, number)
                textWrongCode.visibility = View.INVISIBLE
            }
            1 -> showNumber(ellipse2, number2, number)
            2 -> showNumber(ellipse3, number3, number)
            3 -> {
                showNumber(ellipse4, number4, number)
                hideKeyboard()

                sendSmsCode()
            }
        }
    }

    @SuppressLint("CheckResult")
    private fun sendSmsCode() {
        val backendApi = MainObject.mBackendApi ?: return

        val body = AuthBodyCreator.sendSmsCodeBody(mPhoneNumber, userCode)
        backendApi.authApi().sendSmsCode(body)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({
                if (it == null) {
                    wrongCode()
                } else {
                    context?.let { it1 -> TokenPreferences.addAccessToken(it1, it.access_token) }
                    context?.let { it1 -> TokenPreferences.addRefreshToken(it1, it.refresh_token) }
                    MainObject.mCallback?.onAuthorized()
                }
            },{
                wrongCode()
                it.printStackTrace()
            })
    }

    @SuppressLint("CheckResult")
    private fun getNewSmsCode() {
        val backendApi = MainObject.mBackendApi ?: return
        val body = AuthBodyCreator.sendPhoneBody(mPhoneNumber)
        backendApi.authApi().sendPhone(body)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({ },{
                it.printStackTrace()
            })
    }

    private fun wrongCode() {
        editNumbers.text.clear()
        currentNumber = -1
        userCode = ""
        textWrongCode.visibility = View.VISIBLE
        for (i in 0..4) {
            hideEditNumber(i)
        }
    }

    private fun hideEditNumber(edit: Int) {
        when (edit) {
            0 -> hideNumber(ellipse1, number1)
            1 -> hideNumber(ellipse2, number2)
            2 -> hideNumber(ellipse3, number3)
            3 -> hideNumber(ellipse4, number4)
        }
    }

    private fun showNumber(ellips: ImageView, editNumber: TextView, number: String) {
        ellips.visibility = View.INVISIBLE
        editNumber.visibility = View.VISIBLE
        editNumber.text = number
        userCode += number
    }

    private fun hideNumber(ellips: ImageView, editNumber: TextView) {
        ellips.visibility = View.VISIBLE
        editNumber.visibility = View.INVISIBLE
        editNumber.text = "0"
        userCode = userCode.dropLast(1)
    }

    private fun hideKeyboard() {
        try {
            (context as Activity).window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
            if ((context as Activity).currentFocus != null && (context as Activity).currentFocus!!.windowToken != null) {
                (context?.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(
                    (context as Activity).currentFocus!!.windowToken,
                    0
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
}