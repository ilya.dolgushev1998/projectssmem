package com.morozov.feature_authorization_impl

import android.annotation.SuppressLint
import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.morozov.core_backend_api.FeatureBackendApi
import com.morozov.core_backend_api.authorization.AuthBodyCreator
import com.morozov.core_backend_api.token.TokenPreferences
import com.morozov.feature_authorization_api.AuthorizationFeatureApi
import com.morozov.feature_authorization_api.AuthorizationStarter
import com.morozov.feature_authorization_impl.ui.MainObject
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class AuthorizationFeatureImpl(private val starter: AuthorizationStarter,
                               private val context: Context,
                               private val api: FeatureBackendApi): AuthorizationFeatureApi {

    override fun authorizationStarter(): AuthorizationStarter = starter

    @SuppressLint("CheckResult")
    override fun isAuthorized(): MutableLiveData<Boolean> {
        val result = MutableLiveData<Boolean>()

        val refreshToken = TokenPreferences.getRefreshToken(context)
        if (refreshToken == null)
            result.value = false
        else {
            TokenPreferences.addAccessToken(context, "")
            val body = AuthBodyCreator.refreshKeyBody(refreshToken)
            api.authApi().refreshKey(body)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    if (it == null)
                        result.value = false
                    else {
                        result.value = true
                        TokenPreferences.addAccessToken(context, it.access_token)
                        TokenPreferences.addRefreshToken(context, it.refresh_token)
                    }
                },{
                    result.value = false
                    it.printStackTrace()
                })
        }

        return result
    }
}