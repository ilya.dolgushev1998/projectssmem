package com.morozov.feature_authorization_impl.start

import androidx.fragment.app.FragmentManager
import androidx.navigation.fragment.NavHostFragment
import com.morozov.core_backend_api.FeatureBackendApi
import com.morozov.feature_authorization_api.AuthorizationStarter
import com.morozov.feature_authorization_api.FeatureAuthorizationCallback
import com.morozov.feature_authorization_impl.R
import com.morozov.feature_authorization_impl.ui.MainObject
import com.morozov.feature_authorization_impl.ui.fragments.phone.PhoneNumberFragment
import com.morozov.feature_authorization_impl.ui.fragments.sms.SmsCodeFragment

class AuthorizationStarterImpl: AuthorizationStarter {

    override fun start(manager: FragmentManager, container: Int, addToBackStack: Boolean,
                       callback: FeatureAuthorizationCallback, backendApi: FeatureBackendApi) {
        MainObject.mCallback = callback
        MainObject.mBackendApi = backendApi
        val finalHost = NavHostFragment.create(R.navigation.nav_host)
        manager.beginTransaction()
            .replace(container, finalHost)
            .setPrimaryNavigationFragment(finalHost)
            .commitAllowingStateLoss()
    }
}