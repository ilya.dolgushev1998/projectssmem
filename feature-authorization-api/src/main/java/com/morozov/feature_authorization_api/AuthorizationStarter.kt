package com.morozov.feature_authorization_api

import androidx.fragment.app.FragmentManager
import com.morozov.core_backend_api.FeatureBackendApi

interface AuthorizationStarter {

    fun start(manager: FragmentManager, container: Int, addToBackStack: Boolean,
              callback: FeatureAuthorizationCallback, backendApi: FeatureBackendApi)
}