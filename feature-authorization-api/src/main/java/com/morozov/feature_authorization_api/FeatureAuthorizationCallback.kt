package com.morozov.feature_authorization_api

interface FeatureAuthorizationCallback {

    fun onAuthorized()
}