package com.morozov.feature_authorization_api

import androidx.lifecycle.MutableLiveData

interface AuthorizationFeatureApi {

    fun authorizationStarter(): AuthorizationStarter
    fun isAuthorized(): MutableLiveData<Boolean>
}