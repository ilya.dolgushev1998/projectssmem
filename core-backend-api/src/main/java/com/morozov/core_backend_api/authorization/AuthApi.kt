package com.morozov.core_backend_api.authorization

import com.morozov.core_backend_api.authorization.models.TokenModel
import io.reactivex.Single
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthApi {
    @POST("auth/sendCode/")
    fun sendPhone(@Body body: RequestBody): Single<Any>

    @POST("auth/verifyCode/")
    fun sendSmsCode(@Body body: RequestBody): Single<TokenModel?>

    @POST("auth/refresh/")
    fun refreshKey(@Body body: RequestBody): Single<TokenModel?>
}