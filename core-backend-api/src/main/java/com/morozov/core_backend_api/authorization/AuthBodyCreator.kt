package com.morozov.core_backend_api.authorization

import okhttp3.FormBody
import okhttp3.RequestBody

object AuthBodyCreator {

    fun sendPhoneBody(number: String): RequestBody {
        return FormBody.Builder().add("number", number).build()
    }

    fun sendSmsCodeBody(number: String, code: String): RequestBody {
        return FormBody.Builder()
            .add("number", number)
            .add("code", code)
            .build()
    }

    fun refreshKeyBody(refresh_token: String,
                       grant_type: String = "refresh_token",
                       client_id: String = "null",
                       client_secret: String = "null"): RequestBody {
        return FormBody.Builder()
            .add("refresh_token", refresh_token)
            .add("grant_type", grant_type)
            .add("client_id", client_id)
            .add("client_secret", client_secret)
            .build()
    }
}