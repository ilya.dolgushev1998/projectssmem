package com.morozov.core_backend_api.authorization.models

class TokenModel {
    lateinit var access_token: String
    lateinit var refresh_token: String
}