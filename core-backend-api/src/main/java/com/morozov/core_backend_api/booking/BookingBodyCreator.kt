package com.morozov.core_backend_api.booking

import okhttp3.FormBody
import okhttp3.RequestBody

object BookingBodyCreator {
    fun insertOrderBody(orLat: Double, orLng: Double,
                        endLat: Double, endLng: Double,
                        startCity: Int = 1, endCity: Int = 1): RequestBody {
        return FormBody.Builder()
            .add("origin-lat", orLat.toString())
            .add("origin-lng", orLng.toString())
            .add("destination-lat", endLat.toString())
            .add("destination-lng", endLng.toString())
            .add("start-city", startCity.toString())
            .add("end-city", endCity.toString())
            .build()
    }
}