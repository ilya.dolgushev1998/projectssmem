package com.morozov.core_backend_api.booking.models

class LastOrderModel {
    lateinit var state: String
    var price: Double? = null
    var pd_first_name: String? = null
    var car_number: String? = null
    var model_name: String? = null
    var pwpd_last_lat: Double? = null
    var pwpd_last_lng: Double? = null
}