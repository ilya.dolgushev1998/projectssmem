package com.morozov.core_backend_api.booking

import com.morozov.core_backend_api.booking.models.InsertModel
import com.morozov.core_backend_api.booking.models.LastOrderModel
import io.reactivex.Single
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

interface BookingApi {
    @GET("query/getDirections/")
    fun getRouts(@Header("origin-lat") orLat: Double,
                 @Header("origin-lng") orLng: Double,
                 @Header("destination-lat") endLat: Double,
                 @Header("destination-lng") endLng: Double): Single<String>

    @GET("query/getLastOrderClient/")
    fun getLastOrderClient(@Header("origin-lat") orLat: Double,
                           @Header("origin-lng") orLng: Double,
                           @Header("destination-lat") endLat: Double,
                           @Header("destination-lng") endLng: Double): Single<LastOrderModel>

    @POST("query/insertOrder/")
    fun insertOrder(@Body body: RequestBody): Single<InsertModel>

    @POST("query/insertOrder/")
    fun insertOrder(@Header("origin-lat") orLat: Double,
                    @Header("origin-lng") orLng: Double,
                    @Header("destination-lat") endLat: Double,
                    @Header("destination-lng") endLng: Double,
                    @Header("start-city") start: Int = 1,
                    @Header("end-city") end: Int = 1): Single<InsertModel>
}