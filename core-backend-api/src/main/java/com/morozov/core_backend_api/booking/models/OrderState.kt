package com.morozov.core_backend_api.booking.models

enum class OrderState {
    WAITING,
    TRIP,
    TRIP_TO_CLIENT,
    FINDING,
    FINISHED,
    PRELIMINARY
}