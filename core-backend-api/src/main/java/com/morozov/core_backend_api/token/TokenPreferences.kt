package com.morozov.core_backend_api.token

import android.content.Context
import android.preference.PreferenceManager

object TokenPreferences {
    private const val ACCESS_TOKEN = "access_token"
    private const val REFRESH_TOKEN = "refresh_token"

    fun addAccessToken(context: Context, token: String) {
        setPreference(context, ACCESS_TOKEN, token)
    }
    fun addRefreshToken(context: Context, token: String) {
        setPreference(context, REFRESH_TOKEN, token)
    }
    fun getAccessToken(context: Context): String? = getStrPreference(context, ACCESS_TOKEN)

    fun getRefreshToken(context: Context): String? = getStrPreference(context, REFRESH_TOKEN)

    private fun setPreference(context: Context, pref: String, value: String) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = preferences.edit()
        editor.putString(pref, value)
        editor.apply()
    }

    private fun getStrPreference(context: Context, pref: String): String? {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        val string = preferences.getString(pref, null) ?: return null
        if (string.isEmpty())
            return null
        return string
    }
}