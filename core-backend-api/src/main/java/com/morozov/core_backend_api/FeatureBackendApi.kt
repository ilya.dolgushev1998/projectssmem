package com.morozov.core_backend_api

import com.morozov.core_backend_api.authorization.AuthApi
import com.morozov.core_backend_api.booking.BookingApi

interface FeatureBackendApi {
    fun authApi(): AuthApi
    fun bookingApi(): BookingApi
}