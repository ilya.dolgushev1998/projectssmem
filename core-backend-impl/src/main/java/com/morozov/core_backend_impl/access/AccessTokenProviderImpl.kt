package com.morozov.core_backend_impl.access

import android.content.Context
import com.morozov.core_backend_api.token.TokenPreferences

class AccessTokenProviderImpl(private val context: Context): AccessTokenProvider {
    override fun token(): String? {
        return TokenPreferences.getAccessToken(context)
    }

    override fun refreshToken(): String? {
        return TokenPreferences.getRefreshToken(context)
    }
}