package com.morozov.core_backend_impl.access

import okhttp3.Interceptor
import okhttp3.Response

class AccessTokenInterceptor(
    private val tokenProvider: AccessTokenProvider
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val token = tokenProvider.token()

        val authenticatedRequest = chain.request().newBuilder()

        authenticatedRequest
            .addHeader("Access-Control-Allow-Origin", "*")
            .addHeader("content-type", "application/x-www-form-urlencoded")

        return if (token == null) {
            chain.proceed(authenticatedRequest.build())
        } else {
            authenticatedRequest
                .addHeader("Authorization", "Bearer $token")

            chain.proceed(authenticatedRequest.build())
        }
    }
}