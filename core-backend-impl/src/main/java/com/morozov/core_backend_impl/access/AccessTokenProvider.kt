package com.morozov.core_backend_impl.access

interface AccessTokenProvider {

    fun token(): String?

    fun refreshToken(): String?
}