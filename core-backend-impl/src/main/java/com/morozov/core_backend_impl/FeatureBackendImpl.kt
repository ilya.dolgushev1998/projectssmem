package com.morozov.core_backend_impl

import com.morozov.core_backend_api.FeatureBackendApi
import com.morozov.core_backend_api.authorization.AuthApi
import com.morozov.core_backend_api.booking.BookingApi
import com.morozov.core_backend_impl.network.NetworkModule
import okhttp3.Interceptor

class FeatureBackendImpl(private val interceptor: Interceptor): FeatureBackendApi {

    override fun authApi(): AuthApi {
        return NetworkModule.retrofit(interceptor).create(AuthApi::class.java)
    }

    override fun bookingApi(): BookingApi {
        return NetworkModule.retrofit(interceptor).create(BookingApi::class.java)
    }
}