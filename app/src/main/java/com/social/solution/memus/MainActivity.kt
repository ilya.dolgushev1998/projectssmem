package com.social.solution.memus

import android.content.Context
import android.graphics.Rect
import android.os.Bundle
import android.view.MotionEvent
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.morozov.core_backend_api.FeatureBackendApi
import com.morozov.feature_authorization_api.AuthorizationFeatureApi
import com.morozov.feature_authorization_api.FeatureAuthorizationCallback
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.generic.instance
import java.util.*

class MainActivity : AppCompatActivity(), KodeinAware {
    companion object {
        const val MAX_CLICK_DURATION = 150
        var startClickTime: Long = 0
        var startClickX: Float = 0f
        var startClickY: Float = 0f
    }

    override val kodein: Kodein = App.kodein

    private val authorizationFeatureApi: AuthorizationFeatureApi by instance()
    private val backendFeatureApi: FeatureBackendApi by instance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        authorizationFeatureApi.isAuthorized().observe(this, androidx.lifecycle.Observer {
            when (it) {
                false -> startAuthorizationFeature()
            }
        })
    }


    private fun startAuthorizationFeature() {
        authorizationFeatureApi.authorizationStarter().start(
            supportFragmentManager, R.id.contentMain, false,
            object : FeatureAuthorizationCallback {
                override fun onAuthorized() {
                    Toast.makeText(baseContext, "", Toast.LENGTH_SHORT).show()
                }
            },
            backendFeatureApi
        )
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (ev != null) {
            when (ev.action) {
                MotionEvent.ACTION_DOWN -> {
                    startClickTime = Calendar.getInstance().timeInMillis
                    startClickX = ev.rawX
                    startClickY = ev.rawY
                }
                MotionEvent.ACTION_UP -> {
                    val clickDuration = Calendar.getInstance().timeInMillis - startClickTime

                    if (clickDuration < MAX_CLICK_DURATION && startClickX == ev.rawX && startClickY == ev.rawY) {
                        val v = currentFocus
                        if (v is EditText) {
                            val outRect = Rect()
                            v.getGlobalVisibleRect(outRect)
                            if (!outRect.contains(ev.rawX.toInt(), ev.rawY.toInt())) {
                                v.clearFocus()
                                val imm =
                                    getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                                imm.hideSoftInputFromWindow(v.windowToken, 0)
                            }
                        }
                    }
                }
            }
        }
        return super.dispatchTouchEvent(ev)
    }

    private fun clearBackStack() {
        for (i in 0..supportFragmentManager.backStackEntryCount) {
            supportFragmentManager.popBackStack()
        }
    }
}

