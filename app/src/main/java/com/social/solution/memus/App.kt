package com.social.solution.memus

import android.app.Application
import com.morozov.core_backend_api.FeatureBackendApi
import com.morozov.core_backend_impl.FeatureBackendImpl
import com.morozov.core_backend_impl.access.AccessTokenInterceptor
import com.morozov.core_backend_impl.access.AccessTokenProviderImpl
import com.morozov.feature_authorization_api.AuthorizationFeatureApi
import com.morozov.feature_authorization_api.AuthorizationStarter
import com.morozov.feature_authorization_impl.AuthorizationFeatureImpl
import com.morozov.feature_authorization_impl.start.AuthorizationStarterImpl
import org.kodein.di.Kodein
import org.kodein.di.LazyKodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

class App : Application() {

    companion object {
        lateinit var kodein: LazyKodein
    }

    override fun onCreate() {
        super.onCreate()

        val cBackendModule = Kodein.Module("BackendModule") {
            bind<FeatureBackendApi>() with singleton {
                FeatureBackendImpl(
                    AccessTokenInterceptor(
                        AccessTokenProviderImpl(applicationContext)
                    )
                )
            }
        }

        val fAuthorizationModule = Kodein.Module("FeatureAuthorization") {
            bind<AuthorizationStarter>() with singleton { AuthorizationStarterImpl() }
            bind<AuthorizationFeatureApi>() with singleton {
                AuthorizationFeatureImpl(
                    instance(),
                    applicationContext,
                    instance()
                )
            }
        }

        kodein = Kodein.lazy {
            import(fAuthorizationModule)
            import(cBackendModule)
        }
    }
}